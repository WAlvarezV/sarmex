﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sarmex.Modelo
{
    public class VistaCarga
    {
        public int id_producto { get; set; }
        public string producto { get; set; }
        public int ciclo { get; set; }
        public string itinerario { get; set; }
        public string ruta { get; set; }
        public string estado { get; set; }
        public string municipo { get; set; }
        public string direccion { get; set; }
        public string cliente { get; set; }
        public string telefono { get; set; }
        public int id_usuario { get; set; }
        public string fecha_carga { get; set; }
        public string funcionario { get; set; }
        public int id_carga{ get; set; }
        public VistaCarga ()
        {

        }

        public VistaCarga(int id_producto, string producto, int ciclo, string itinerario, string ruta, string estado, string municipo,
                            string direccion, string cliente, string telefono, int id_usuario, string fecha_carga, string funcionario, int id_carga)
        {
            this.id_producto = id_producto;
            this.producto = producto;
            this.ciclo = ciclo;
            this.itinerario = itinerario;
            this.ruta = ruta;
            this.estado = estado;
            this.municipo = municipo;
            this.direccion = direccion;
            this.cliente = cliente;
            this.telefono = telefono;
            this.id_usuario = id_usuario;
            this.fecha_carga = fecha_carga;
            this.funcionario = funcionario;
            this.id_carga = id_carga;
        }
    }
}