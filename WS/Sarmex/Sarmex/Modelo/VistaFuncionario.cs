﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sarmex.Modelo
{
    public class VistaFuncionario
    {
        public int id_funcionario { get; set; }
        public string funcionario { get; set; }
        public int cargados { get; set; }
        public int ejecutados { get; set; }

        public VistaFuncionario()
        {
        }

        public VistaFuncionario(int id_funcionario, string funcionario, int cargados, int ejecutados)
        {
            this.id_funcionario = id_funcionario;
            this.funcionario = funcionario;
            this.cargados = cargados;
            this.ejecutados = ejecutados;
        }
    }
}