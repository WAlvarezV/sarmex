﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sarmex.Modelo
{
    public class Medidor
    {
        public int id_medidor { get; set; }
        public int clase { get; set; }
        public int tipo_medidor { get; set; }
        public string marca { get; set; }
        public string serial { get; set; }
        public int anio_fab { get; set; }
        public int digitos { get; set; }

        public Medidor()
        {
            this.id_medidor = 0;
            this.clase = 0;
            this.tipo_medidor = 0;
            this.marca = "";
            this.serial = "";
            this.anio_fab = 0;
            this.digitos = 0;

        }

        public Medidor(int id_medidor, int clase, int tipo_medidor, string marca,string serial,int anio_fab,int digitos)
        {
            this.id_medidor = id_medidor;
            this.clase = clase;
            this.tipo_medidor = tipo_medidor;
            this.marca = marca;
            this.serial = serial;
            this.anio_fab = anio_fab;
            this.digitos = digitos;
        }
    }
}