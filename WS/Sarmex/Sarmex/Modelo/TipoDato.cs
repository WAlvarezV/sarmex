﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sarmex.Modelo
{
    public class TipoDato
    {
        public int id_tipo { get; set; }
        public string grupo { get; set; }
        public string codigo { get; set; }
        public string descripcion { get; set; }

        public TipoDato()
        {
            this.id_tipo = 0;
            this.grupo = "";
            this.codigo = "";
            this.descripcion = "";
        }
        public TipoDato(int id_tipo, string grupo, string codigo, string descripcion)
        {
            this.id_tipo = id_tipo;
            this.grupo = grupo;
            this.codigo = codigo;
            this.descripcion = descripcion;
        }
    }
}