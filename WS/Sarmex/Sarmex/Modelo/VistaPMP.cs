﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sarmex.Modelo
{
    public class VistaPMP
    {
        public int id_producto { get; set; }
        public string producto { get; set; }
        public string municipio { get; set; }
        public string direccion { get; set; }
        public string cliente { get; set; }
        public string telefono { get; set; }
        public string marca { get; set; }
        public string serial { get; set; }
        public int anio_fab { get; set; }
        public int digitos { get; set; }
        public int id_medidor { get; set; }
        public string clase { get; set; }
        public string tpo_medidor { get; set; }

        public VistaPMP()
        {

        }
        public VistaPMP(int id_producto,
                        string producto,
                        string municipo,
                        string direccion,
                        string cliente,
                        string telefono,
                        string marca,
                        string serial,
                        int anio_fab,
                        int digitos,
                        int id_medidor,
                        string clase,
                        string tpo_medidor
                        )
        {
            this.id_producto = id_producto;
            this.producto = producto;
            this.municipio = municipo;
            this.direccion = direccion;
            this.cliente = cliente;
            this.telefono = telefono;
            this.marca = marca;
            this.serial = serial;
            this.anio_fab = anio_fab;
            this.digitos = digitos;
            this.id_medidor = id_medidor;
            this.clase = clase;
            this.tpo_medidor = tpo_medidor;


        }
    }
}