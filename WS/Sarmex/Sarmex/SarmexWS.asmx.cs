﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Services;
using MySql.Data.MySqlClient;
using Sarmex.Modelo;

namespace Sarmex
{
    /// <summary>
    /// Descripción breve de SarmexWS
    /// </summary>
    [WebService(Namespace = "http://sgoliver.net/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Para permitir que se llame a este servicio web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente. 
    // [System.Web.Script.Services.ScriptService]
    public class SarmexWS : System.Web.Services.WebService
    {
        MySqlConnection conn = new MySqlConnection(ConfigurationManager.ConnectionStrings["cadenaConexion"].ConnectionString);

       

        [WebMethod]
        public TipoDato[] listadoTipoDatos()
        {

            conn.Open();
            string sql = "select * from `tipo_dato`";
            MySqlCommand cmd = new MySqlCommand(sql, conn);

            MySqlDataReader reader = cmd.ExecuteReader();

            List<TipoDato> lista = new List<TipoDato>();

            while (reader.Read())
            {
                lista.Add(
                    new TipoDato(reader.GetInt32(0),
                                reader.GetString(1),
                                reader.GetString(2),
                                reader.GetString(3)
                                ));
            }

            conn.Close();

            return lista.ToArray();

        }

        [WebMethod]
        public VistaCarga[] listarCarga(int id_usuario)
        {

            conn.Open();
            string sql = "select * from view_carga where id_usuario = " + id_usuario+ " and fecha_carga = (select curdate())";
            MySqlCommand cmd = new MySqlCommand(sql, conn);

            MySqlDataReader reader = cmd.ExecuteReader();

            List<VistaCarga> lista = new List<VistaCarga>();

            while (reader.Read())
            {
                lista.Add(new VistaCarga( reader.GetInt32(0),
                                    reader.GetString(1),
                                    reader.GetInt32(2),
                                    reader.GetString(3),
                                    reader.GetString(4),
                                    reader.GetString(5),
                                    reader.GetString(6),
                                    reader.GetString(7),
                                    reader.GetString(8),
                                    reader.GetString(9),
                                    reader.GetInt32(10),
                                    reader.GetString(11),
                                    reader.GetString(12),
                                    reader.GetInt32(13)
                                ));
            }

            conn.Close();

            return lista.ToArray();

        }

        [WebMethod]
        public VistaPMP[] medidorCarga(int id_producto)
        {

            conn.Open();
            string sql = "select * from view_pmp where id_producto = " + id_producto;
            MySqlCommand cmd = new MySqlCommand(sql, conn);

            MySqlDataReader reader = cmd.ExecuteReader();

            List<VistaPMP> lista = new List<VistaPMP>();

            while (reader.Read())
            {
                lista.Add(new VistaPMP(reader.GetInt32(0),
                                    reader.GetString(1),
                                    reader.GetString(2),
                                    reader.GetString(3),
                                    reader.GetString(4),
                                    reader.GetString(5),
                                    reader.GetString(6),
                                    reader.GetString(7),
                                    reader.GetInt32(8),
                                    reader.GetInt32(9),
                                    reader.GetInt32(10),
                                    reader.GetString(11),
                                    reader.GetString(12)
                                ));
            }

            conn.Close();

            return lista.ToArray();

        }

        [WebMethod]
        public VistaFuncionario[] listarFuncionario()
        {
            conn.Open();
            string sql = "select * from view_funcionario";
            MySqlCommand cmd = new MySqlCommand(sql, conn);
            MySqlDataReader reader = cmd.ExecuteReader();
            List<VistaFuncionario> lista = new List<VistaFuncionario>();
            while (reader.Read())
            {
                lista.Add(new VistaFuncionario(reader.GetInt32(0),
                                    reader.GetString(1),
                                    reader.GetInt32(2),
                                    reader.GetInt32(3)
                                ));
            }

            conn.Close();
            return lista.ToArray();
        }

        [WebMethod]
        public int loginUsuario(string usuario, string clave)
        {
            conn.Open();
            int id_usuario = -1;
            int tpo_usuario = -1;
            String sql = " SELECT u.`USUARIO`, u.`TPO_USR` FROM `usuario` u WHERE u.`USUARIO` = " + usuario+" AND u.`PASS` = MD5("+clave+ ") AND u.`ESTADO` = 1";
            MySqlCommand cmd = new MySqlCommand(sql, conn);
            MySqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                id_usuario = reader.GetInt32(0);
                tpo_usuario = reader.GetInt32(1);
            }
            if (id_usuario != -1 && tpo_usuario == 15)
            {
                conn.Close();
                return 1;
            }
            else if (id_usuario != -1 && tpo_usuario == 16)
            {
                conn.Close();
                return id_usuario;
            }
            else
                conn.Close();
            return -1;
        }

        [WebMethod]
        public Boolean editarMedidor(int id_medidor, int clase, int tipo_medidor, string marca, string serial, int anio_fab, int digitos, int id_carga)
        {
            conn.Open();
            String sql = String.Format("UPDATE MEDIDOR SET CLASE = {1}, TIPO_MEDIDOR = {2}, MARCA = '{3}', SERIAL = '{4}', ANIO_FAB = {5}, DIGITOS = {6} WHERE ID_MEDIDOR = {0}", id_medidor, clase, tipo_medidor, marca, serial, anio_fab, digitos);
            MySqlCommand comando = new MySqlCommand(sql, conn);
            if (comando.ExecuteNonQuery() > 0)
            {
                actualizarCarga(id_carga);
                conn.Close();
                return true;
            }
            else
                return false;
        }

        public Boolean actualizarCarga(int id_carga)
        {
            
            String sql = String.Format("UPDATE CARGA SET ESTADO = {0} WHERE ID_CARGA = {1}", 26, id_carga);
            MySqlCommand comando = new MySqlCommand(sql, conn);
            if (comando.ExecuteNonQuery() > 0)
            {
                return true;
            }
            else
                return false;
        }

        [WebMethod]
        public TipoDato buscarTipoDato(int id_tipo)
        {
            conn.Open();
            string sql = "select * from `tipo_dato` where id_tipo = "+id_tipo;
            MySqlCommand cmd = new MySqlCommand(sql, conn);
            TipoDato res = null;
            MySqlDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                res = (new TipoDato(reader.GetInt32(0),
                                reader.GetString(1),
                                reader.GetString(2),
                                reader.GetString(3)
                                ));
            }

            conn.Close();

            return res;

        }
    }
}
