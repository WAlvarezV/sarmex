package com.example.lucho.sarmex;

import android.app.AlertDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import android.view.View;
import android.view.View.OnClickListener;

import com.example.lucho.sarmex.logica.ItemSpinner;
import com.example.lucho.sarmex.medidor.VistaPMP;
import com.example.lucho.sarmex.modelo.carga.ListaCargas;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.util.LinkedList;

public class EditarMedidor extends AppCompatActivity {

    int id_producto,id_carga, id_usuario, id_medidor, clase, tipo_medidor, anio_fab, digitos;
    String	marca ,serial;

    VistaPMP viMedidor;
    TextView txtProducto, txtNombreCliente,txtDireccion;
    EditText txtSerial, txtAnio, txtDigitos;
    Spinner clases,tipos, marcas;
    final String NAMESPACE = "http://sgoliver.net/";
    final String URL = "http://"+ Constantes.IP+"/SarmexWS.asmx";
    ItemSpinner spinnerlist = new ItemSpinner();
    Button btnEditar, btnGuardar;
    ArrayAdapter adapter_clases, adapter_marcas,adapter_tipos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editar_medidor);

        Bundle bundle = getIntent().getExtras();
        id_producto = bundle.getInt("IDP");
        id_carga = bundle.getInt("IDC");
        id_usuario = bundle.getInt("IDU");

        clases = (Spinner) findViewById(R.id.spnClase);
        LinkedList listaclases = spinnerlist.ClasesMedidor();
        adapter_clases = new ArrayAdapter(this, android.R.layout.simple_list_item_1, listaclases );
        adapter_clases.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        clases.setAdapter(adapter_clases);

        marcas = (Spinner) findViewById(R.id.spnMarca);
        LinkedList listamarcas = spinnerlist.Marcas();
        adapter_marcas = new ArrayAdapter(this, android.R.layout.simple_list_item_1, listamarcas );
        adapter_marcas.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        marcas.setAdapter(adapter_marcas);


        tipos = (Spinner) findViewById(R.id.spnTM);
        LinkedList listatipos = spinnerlist.TipoMedidor();
        adapter_tipos = new ArrayAdapter(this, android.R.layout.simple_list_item_1, listatipos );
        adapter_tipos.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        tipos.setAdapter(adapter_tipos);

        txtProducto = (TextView) findViewById(R.id.txtProducto);
        txtNombreCliente = (TextView) findViewById(R.id.txtNombreCliente);
        txtDireccion = (TextView) findViewById(R.id.txtDireccion);
        txtSerial = (EditText) findViewById(R.id.txtSerial);
        txtAnio = (EditText) findViewById(R.id.txtAño);
        txtDigitos = (EditText) findViewById(R.id.txtDigitos);

        btnEditar = (Button) findViewById(R.id.btnEditar);

        btnEditar.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                txtSerial.setEnabled(true);
                txtAnio.setEnabled(true);
                txtDigitos.setEnabled(true);
                marcas.setEnabled(true);
                clases.setEnabled(true);
                tipos.setEnabled(true);
            }
        });

        btnGuardar = (Button) findViewById(R.id.btnGuardar);
        btnGuardar.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                ActualizarMedidor actualizarMedidor = new ActualizarMedidor();
                actualizarMedidor.execute();
            }
        });
        ConsultaMedidor consultaMedidor = new ConsultaMedidor();
        consultaMedidor.execute();
    }

    //Tarea As�ncrona para llamar al WS de consulta en segundo plano
    private class ConsultaMedidor extends AsyncTask<String,Integer,Boolean> {
        private VistaPMP[] listaPMP;
        protected Boolean doInBackground(String... params) {
            boolean resul = true;
            final String METHOD_NAME = "medidorCarga";
            final String SOAP_ACTION = "http://sgoliver.net/medidorCarga";
            SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);

            request.addProperty("id_producto", id_producto);

            SoapSerializationEnvelope envelope =  new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.dotNet = true;
            envelope.setOutputSoapObject(request);
            HttpTransportSE transporte = new HttpTransportSE(URL);
            try
            {
                transporte.call(SOAP_ACTION, envelope);
                SoapObject resSoap =(SoapObject)envelope.getResponse();
                listaPMP = new VistaPMP[resSoap.getPropertyCount()];

                SoapObject ic = (SoapObject)resSoap.getProperty(0);

                viMedidor = new VistaPMP();

                viMedidor.setId_producto(Integer.parseInt(ic.getProperty(0).toString()));
                viMedidor.setProducto(ic.getProperty(1).toString());
                viMedidor.setMunicipio(ic.getProperty(2).toString());
                viMedidor.setDireccion(ic.getProperty(3).toString());
                viMedidor.setCliente(ic.getProperty(4).toString());
                viMedidor.setTelefono(ic.getProperty(5).toString());
                viMedidor.setMarca(ic.getProperty(6).toString());
                viMedidor.setSerial(ic.getProperty(7).toString());
                viMedidor.setAnio_fab(Integer.parseInt(ic.getProperty(8).toString()));
                viMedidor.setDigitos(Integer.parseInt(ic.getProperty(9).toString()));
                viMedidor.setId_medidor(Integer.parseInt(ic.getProperty(10).toString()));
                viMedidor.setClase(ic.getProperty(11).toString());
                viMedidor.setTpo_medidor(ic.getProperty(12).toString());


            }
            catch (Exception e)
            {
                Log.i("Errores", e.toString());
                resul = false;
            }
            return resul;
        }

        protected void onPostExecute(Boolean result) {

            if (result)
            {
                id_medidor = viMedidor.getId_medidor();
                txtProducto.setText("Producto: " + viMedidor.getProducto());
                txtDireccion.setText("Direccion: " + viMedidor.getDireccion());
                txtNombreCliente.setText("Cliente: " + viMedidor.getCliente());
                txtSerial.setText(viMedidor.getSerial());
                txtAnio.setText("" + viMedidor.getAnio_fab());
                txtDigitos.setText("" + viMedidor.getDigitos());

                txtSerial.setEnabled(false);
                txtAnio.setEnabled(false);
                txtDigitos.setEnabled(false);
                int index = spinnerlist.slectedMarca(viMedidor.getMarca());
                marcas.setSelection(index, true);
                marcas.setEnabled(false);
                index = spinnerlist.slectedClase(viMedidor.getClase());
                clases.setSelection(index, false);
                clases.setEnabled(false);
                index = spinnerlist.slectedTipo(viMedidor.getTpo_medidor());
                tipos.setSelection(index, false);
                tipos.setEnabled(false);
            }
            else
            {
                AlertDialog.Builder alerta = new AlertDialog.Builder(EditarMedidor.this);
                alerta.setTitle("Error: ");
                alerta.setMessage("No hay datos del Medidor");
                alerta.setPositiveButton("Atras", null);
                alerta.show();
            }
        }
    }

    //Tarea As�ncrona para llamar al WS de consulta en segundo plano
    private class ActualizarMedidor extends AsyncTask<String,Integer,Boolean> {

        protected Boolean doInBackground(String... params) {

            boolean resul = true;

            final String METHOD_NAME = "editarMedidor";
            final String SOAP_ACTION = "http://sgoliver.net/editarMedidor";
            SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);

            request.addProperty("id_medidor", id_medidor);
            request.addProperty("clase", clase);
            request.addProperty("tipo_medidor", tipo_medidor);
            request.addProperty("marca", marca);
            request.addProperty("serial", serial);
            request.addProperty("anio_fab", anio_fab);
            request.addProperty("digitos", digitos);
            request.addProperty("id_carga", id_carga);

            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);

            envelope.dotNet = true;

            envelope.setOutputSoapObject(request);

            HttpTransportSE transporte = new HttpTransportSE(URL);

            try
            {
                transporte.call(SOAP_ACTION, envelope);

                SoapPrimitive resultado_xml =(SoapPrimitive)envelope.getResponse();
                String res = resultado_xml.toString();

                if(!res.equals("true"))
                    resul = false;
            }
            catch (Exception e)
            {
                Log.i("Errores", e.toString());
                resul = false;
            }

            return resul;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            ItemSpinner itemSelected = (ItemSpinner)marcas.getSelectedItem();
            marca = itemSelected.toString();

            itemSelected = (ItemSpinner)clases.getSelectedItem();
            clase = itemSelected.getId();

            itemSelected = (ItemSpinner)tipos.getSelectedItem();
            tipo_medidor = itemSelected.getId();


            serial = txtSerial.getText().toString();
            anio_fab = Integer.parseInt(txtAnio.getText().toString());
            digitos = Integer.parseInt(txtDigitos.getText().toString());

        }

        protected void onPostExecute(Boolean result) {

            if (result)
            {
                AlertDialog.Builder alerta = new AlertDialog.Builder(EditarMedidor.this);
                alerta.setTitle("Confirmación");
                alerta.setMessage("Datos del Medidor Actualizados y Confirmados");
                alerta.setPositiveButton("Atras", null);
                alerta.show();
                txtSerial.setEnabled(false);
                txtAnio.setEnabled(false);
                txtDigitos.setEnabled(false);
                marcas.setEnabled(false);
                clases.setEnabled(false);
                tipos.setEnabled(false);


            }
            else
            {
                AlertDialog.Builder alerta = new AlertDialog.Builder(EditarMedidor.this);
                alerta.setTitle("Error: ");
                alerta.setMessage("Datos del Medidor NO Actualizados");
                alerta.setPositiveButton("Atras", null);
                alerta.show();
            }
        }
    }


}
