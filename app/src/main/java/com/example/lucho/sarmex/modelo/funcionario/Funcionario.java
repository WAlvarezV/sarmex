package com.example.lucho.sarmex.modelo.funcionario;

/**
 * Created by lucho on 29/05/2016.
 */
public class Funcionario {
    private int id_funcionario ;
    private String funcionario ;
    private int cargados ;
    private int ejecutados ;

    public int getId_funcionario() {
        return id_funcionario;
    }

    public void setId_funcionario(int id_funcionario) {
        this.id_funcionario = id_funcionario;
    }

    public String getFuncionario() {
        return funcionario;
    }

    public void setFuncionario(String funcionario) {
        this.funcionario = funcionario;
    }

    public int getCargados() {
        return cargados;
    }

    public void setCargados(int cargados) {
        this.cargados = cargados;
    }

    public int getEjecutados() {
        return ejecutados;
    }

    public void setEjecutados(int ejecutados) {
        this.ejecutados = ejecutados;
    }
}
