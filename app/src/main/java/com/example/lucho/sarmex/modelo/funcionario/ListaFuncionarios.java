package com.example.lucho.sarmex.modelo.funcionario;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.TextView;

import com.example.lucho.sarmex.Constantes;
import com.example.lucho.sarmex.R;
import com.example.lucho.sarmex.modelo.carga.ListaCargas;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

public class ListaFuncionarios extends AppCompatActivity {

    final String NAMESPACE = "http://sgoliver.net/";
    final String URL = "http://"+ Constantes.IP+"/SarmexWS.asmx";
    public AdaptadorFuncionario adaptadorFuncionario;
    int id_usuario;
    ListView ListaFuncionarios;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_funcionarios);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ListaFuncionarios = (ListView) findViewById(R.id.listViewFuncionarios);
        ListaFuncionarios.setOnItemClickListener(listenerLista);
        ListarFuncionarios listarFuncionarios = new ListarFuncionarios();
        listarFuncionarios.execute();
    }

    protected void onRestart() {
        super.onRestart();
        ListarFuncionarios listarFuncionarios = new ListarFuncionarios();
        listarFuncionarios.execute();
    }



    OnItemClickListener listenerLista = new AdapterView.OnItemClickListener() {

        @Override
        public void onItemClick(AdapterView<?> arg0, View view, int posicion,
                                long arg3) {
            // TODO Auto-generated method stub
                TextView txtIdFuncionario = (TextView) findViewById(R.id.txtIdFuncionario);
                id_usuario = Integer.parseInt(txtIdFuncionario.getText().toString());
                Intent res = new Intent(ListaFuncionarios.this, ListaCargas.class);
                res.putExtra("ID", id_usuario);
                res.putExtra("IDA", 1);
                startActivity(res);
        }
    };

    //Tarea As�ncrona para llamar al WS de consulta en segundo plano
    private  class ListarFuncionarios extends AsyncTask<String,Integer,Boolean> {
        private Funcionario[] listaFuncionario;
        protected Boolean doInBackground(String... params) {
            boolean resul = true;
            final String METHOD_NAME = "listarFuncionario";
            final String SOAP_ACTION = "http://sgoliver.net/listarFuncionario";
            SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);

            SoapSerializationEnvelope envelope =  new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.dotNet = true;
            envelope.setOutputSoapObject(request);
            HttpTransportSE transporte = new HttpTransportSE(URL);
            try
            {
                transporte.call(SOAP_ACTION, envelope);
                SoapObject resSoap =(SoapObject)envelope.getResponse();
                listaFuncionario = new Funcionario[resSoap.getPropertyCount()];
                for (int i = 0; i < listaFuncionario.length; i++)
                {
                    SoapObject ic = (SoapObject)resSoap.getProperty(i);

                    Funcionario funcionario = new Funcionario();

                    funcionario.setId_funcionario(Integer.parseInt(ic.getProperty(0).toString()));
                    funcionario.setFuncionario(ic.getProperty(1).toString());
                    funcionario.setCargados(Integer.parseInt(ic.getProperty(2).toString()));
                    funcionario.setEjecutados(Integer.parseInt(ic.getProperty(3).toString()));

                    listaFuncionario[i] = funcionario;
                }

            }
            catch (Exception e)
            {
                Log.i("Errores", e.toString());
                resul = false;
            }
            return resul;
        }

        protected void onPostExecute(Boolean result) {

            if (result)
            {
                adaptadorFuncionario = new AdaptadorFuncionario(ListaFuncionarios.this, listaFuncionario);
                ListaFuncionarios.setAdapter(adaptadorFuncionario);
            }
            else
            {
                AlertDialog.Builder alerta = new AlertDialog.Builder(ListaFuncionarios.this);
                alerta.setTitle("Error: ");
                alerta.setMessage("No hay funcionarios");
                alerta.setPositiveButton("Atras", null);
                alerta.show();
            }
        }
    }

}
