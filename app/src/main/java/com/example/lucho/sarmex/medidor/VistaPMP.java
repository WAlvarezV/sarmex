package com.example.lucho.sarmex.medidor;

/**
 * Created by lucho on 22/05/2016.
 */
public class VistaPMP {
    private int id_producto;
    private String producto;
    private String municipio;
    private String direccion;
    private String cliente;
    private String telefono;
    private String marca;
    private String serial;
    private int anio_fab;
    private int digitos;
    private int id_medidor;
    private String clase;
    private String tpo_medidor;

    public int getId_producto() {
        return id_producto;
    }

    public void setId_producto(int id_producto) {
        this.id_producto = id_producto;
    }

    public String getProducto() {
        return producto;
    }

    public void setProducto(String producto) {
        this.producto = producto;
    }

    public String getMunicipio() {
        return municipio;
    }

    public void setMunicipio(String municipio) {
        this.municipio = municipio;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getSerial() {
        return serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }

    public int getAnio_fab() {
        return anio_fab;
    }

    public void setAnio_fab(int anio_fab) {
        this.anio_fab = anio_fab;
    }

    public int getDigitos() {
        return digitos;
    }

    public void setDigitos(int digitos) {
        this.digitos = digitos;
    }

    public int getId_medidor() {
        return id_medidor;
    }

    public void setId_medidor(int id_medidor) {
        this.id_medidor = id_medidor;
    }

    public String getClase() {
        return clase;
    }

    public void setClase(String clase) {
        this.clase = clase;
    }

    public String getTpo_medidor() {
        return tpo_medidor;
    }

    public void setTpo_medidor(String tpo_medidor) {
        this.tpo_medidor = tpo_medidor;
    }
}
