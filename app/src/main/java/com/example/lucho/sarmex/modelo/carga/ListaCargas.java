package com.example.lucho.sarmex.modelo.carga;


import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;

import com.example.lucho.sarmex.Constantes;
import com.example.lucho.sarmex.EditarMedidor;
import com.example.lucho.sarmex.MainActivity;
import com.example.lucho.sarmex.R;
import com.example.lucho.sarmex.modelo.carga.Carga;

public class ListaCargas extends AppCompatActivity {

    ListView ListaCargas;
    int id_usuario;
    int id_producto;
    int id_carga;
    int id_admin = 0;

    TextView funcionario;
    public AdaptadorCarga adaptadorCargas;
    final String NAMESPACE = "http://sgoliver.net/";
    final String URL = "http://"+ Constantes.IP+"/SarmexWS.asmx";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listacargas);

        Bundle bundle = getIntent().getExtras();
        id_usuario = bundle.getInt("ID");
        id_admin = bundle.getInt("IDA");


        funcionario = (TextView)findViewById(R.id.txtFuncionario);
        ListaCargas = (ListView) findViewById(R.id.listViewCarga);
        ListaCargas.setOnItemClickListener(listenerLista);
        ListarCargasFuncionario listarCargas = new ListarCargasFuncionario();
        listarCargas.execute();

    }
    @Override
    protected void onRestart() {
        super.onRestart();
        ListarCargasFuncionario listarCargas = new ListarCargasFuncionario();
        listarCargas.execute();
    }


    OnItemClickListener listenerLista = new OnItemClickListener() {

        @Override
        public void onItemClick(AdapterView<?> arg0, View view, int posicion,
                                long arg3) {
            // TODO Auto-generated method stub

            if(id_admin != 1)
            {
                TextView txtIdproducto = (TextView) view.findViewById(R.id.txtIdProducto);
                TextView txtIdcarga = (TextView) view.findViewById(R.id.txtIdCarga);

                id_producto = Integer.parseInt(txtIdproducto.getText().toString());
                id_carga = Integer.parseInt(txtIdcarga.getText().toString());
                Intent res = new Intent(ListaCargas.this, EditarMedidor.class);
                res.putExtra("IDP", id_producto);
                res.putExtra("IDC", id_carga);
                res.putExtra("IDU", id_usuario);
                startActivity(res);
            }
            else
            {
                AlertDialog.Builder alerta = new AlertDialog.Builder(ListaCargas.this);
                alerta.setTitle("Advertencia!!");
                alerta.setMessage("Para editar el medidor debe estar cargado a su nombre");
                alerta.setPositiveButton("Atras", null);
                alerta.show();
            }

        }
    };

    //Tarea As�ncrona para llamar al WS de consulta en segundo plano
    private  class ListarCargasFuncionario extends AsyncTask<String,Integer,Boolean> {
        private Carga[] listaCarga;
        protected Boolean doInBackground(String... params) {
            boolean resul = true;
            final String METHOD_NAME = "listarCarga";
            final String SOAP_ACTION = "http://sgoliver.net/listarCarga";
            SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);

            request.addProperty("id_usuario", id_usuario);

            SoapSerializationEnvelope envelope =  new SoapSerializationEnvelope(SoapEnvelope.VER11);
            envelope.dotNet = true;
            envelope.setOutputSoapObject(request);
            HttpTransportSE transporte = new HttpTransportSE(URL);
            try
            {
                transporte.call(SOAP_ACTION, envelope);
                SoapObject resSoap =(SoapObject)envelope.getResponse();
                listaCarga = new Carga[resSoap.getPropertyCount()];
                for (int i = 0; i < listaCarga.length; i++)
                {
                    SoapObject ic = (SoapObject)resSoap.getProperty(i);

                    Carga carga = new Carga();

                    carga.setId_producto(Integer.parseInt(ic.getProperty(0).toString()));
                    carga.setProducto(ic.getProperty(1).toString());
                    carga.setCiclo(Integer.parseInt(ic.getProperty(2).toString()));
                    carga.setItinerario(ic.getProperty(3).toString());
                    carga.setRuta(ic.getProperty(4).toString());
                    carga.setEstado(ic.getProperty(5).toString());
                    carga.setMunicipo(ic.getProperty(6).toString());
                    carga.setDireccion(ic.getProperty(7).toString());
                    carga.setCliente(ic.getProperty(8).toString());
                    carga.setTelefono(ic.getProperty(9).toString());
                    carga.setId_usuario(Integer.parseInt(ic.getProperty(10).toString()));
                    carga.setFecha_carga(ic.getProperty(11).toString());
                    carga.setFuncionario(ic.getProperty(12).toString());
                    carga.setId_carga(Integer.parseInt(ic.getProperty(13).toString()));

                    listaCarga[i] = carga;
                }

            }
            catch (Exception e)
            {
                Log.i("Errores", e.toString());
                resul = false;
            }
            return resul;
        }

        protected void onPostExecute(Boolean result) {

            if (result)
            {
                funcionario.setText(listaCarga[0].getFuncionario().toString());
                adaptadorCargas = new AdaptadorCarga(ListaCargas.this, listaCarga);
                ListaCargas.setAdapter(adaptadorCargas);
            }
            else
            {
                AlertDialog.Builder alerta = new AlertDialog.Builder(ListaCargas.this);
                alerta.setTitle("Error: ");
                alerta.setMessage("No hay cargas a la fecha");
                alerta.setPositiveButton("Atras", null);
                alerta.show();
            }
        }
    }





}
