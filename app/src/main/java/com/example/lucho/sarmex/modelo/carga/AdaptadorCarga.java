package com.example.lucho.sarmex.modelo.carga;

import android.app.Activity;
import com.example.lucho.sarmex.R;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

/**
 * Created by lucho on 22/05/2016.
 */
public class AdaptadorCarga  extends ArrayAdapter{

    private Activity context;
    Carga[] datos;

    public AdaptadorCarga(Activity context, Carga[] datos) {
        super(context, R.layout.layout_cargas, datos);
        this.context = context;
        this.datos = datos;
    }

    public View getView(int posicion, View convertView, ViewGroup parent) {
        View item = convertView;
        ViewHolderCarga holder;

        if (item == null) {
            LayoutInflater inflater = context.getLayoutInflater();
            item = inflater.inflate(R.layout.layout_cargas, null);
            holder = new ViewHolderCarga();
            holder.id_producto = (TextView) item.findViewById(R.id.txtIdProducto);
            holder.id_carga = (TextView) item.findViewById(R.id.txtIdCarga);
            holder.producto = (TextView) item.findViewById(R.id.txtProducto);
            holder.ciclo = (TextView) item.findViewById(R.id.txtCiclo);
            holder.itinerario = (TextView) item.findViewById(R.id.txtItinerario);
            holder.Ruta = (TextView) item.findViewById(R.id.txtRuta);
            holder.Estado = (TextView) item.findViewById(R.id.txtEstado);


            item.setTag(holder);
        } else {
            holder = (ViewHolderCarga) item.getTag();
        }
        holder.id_producto.setText(""+datos[posicion].getId_producto());
        holder.id_carga.setText(""+datos[posicion].getId_carga());
        holder.producto.setText("Producto: " + datos[posicion].getProducto());
        holder.ciclo.setText("Ciclo: " + datos[posicion].getCiclo());
        holder.itinerario.setText("Itinerario: "+datos[posicion].getItinerario());
        holder.Ruta.setText("Ruta: "+datos[posicion].getRuta());
        holder.Estado.setText("Estado: "+datos[posicion].getEstado());


        return item;
    }
}
