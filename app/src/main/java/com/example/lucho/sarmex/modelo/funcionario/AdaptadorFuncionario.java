package com.example.lucho.sarmex.modelo.funcionario;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.lucho.sarmex.R;

/**
 * Created by lucho on 29/05/2016.
 */
public class AdaptadorFuncionario extends ArrayAdapter {
    private Activity context;
    Funcionario[] datos;

    public AdaptadorFuncionario(Activity context, Funcionario[] datos) {
        super(context, R.layout.layout_cargas, datos);
        this.context = context;
        this.datos = datos;
    }

    public View getView(int posicion, View convertView, ViewGroup parent) {
        View item = convertView;
        ViewHolderFuncionario holder;

        if (item == null) {
            LayoutInflater inflater = context.getLayoutInflater();
            item = inflater.inflate(R.layout.layout_funcionario, null);
            holder = new ViewHolderFuncionario();
            holder.id_funcionario = (TextView) item.findViewById(R.id.txtIdFuncionario);
            holder.funcionario = (TextView) item.findViewById(R.id.txtNFuncionario);
            holder.cargados = (TextView) item.findViewById(R.id.txtIdCargados);
            holder.ejecutados = (TextView) item.findViewById(R.id.txtIdEjecutados);

            item.setTag(holder);
        } else {
            holder = (ViewHolderFuncionario) item.getTag();
        }
        holder.id_funcionario.setText(""+datos[posicion].getId_funcionario());
        holder.funcionario.setText(datos[posicion].getFuncionario());
        holder.cargados.setText("Cargados: " + datos[posicion].getCargados());
        holder.ejecutados.setText("Ejecutados: " + datos[posicion].getEjecutados());
        return item;
    }
}

