package com.example.lucho.sarmex.logica;

import java.util.LinkedList;

/**
 * Created by lucho on 22/05/2016.
 */
public class ItemSpinner {
    int id;
    String nombre;
    LinkedList marcas;
    LinkedList clasesMedidor;
    LinkedList tipoMedidor;
    //Constructor
    public ItemSpinner(){

    }
    public ItemSpinner(int id, String nombre) {
        super();
        this.id = id;
        this.nombre = nombre;
    }
    @Override
    public String toString() {
        return nombre;
    }
    public int getId() {
        return id;
    }

    public LinkedList ClasesMedidor()
    {
        clasesMedidor = new LinkedList();

        clasesMedidor.add(new ItemSpinner(18, "ELECTRONICO"));
        clasesMedidor.add(new ItemSpinner(19, "MECANICO"));
        clasesMedidor.add(new ItemSpinner(20, "INTELIGENTE"));

        return clasesMedidor;
    }

    public LinkedList TipoMedidor()
    {
        tipoMedidor = new LinkedList();

        tipoMedidor.add(new ItemSpinner(21, "MONOFASICO"));
        tipoMedidor.add(new ItemSpinner(22, "BIFASICO"));
        tipoMedidor.add(new ItemSpinner(23, "TRIFASICO"));

        return tipoMedidor;
    }

    public LinkedList Marcas()
    {
        marcas = new LinkedList();

        marcas.add(new ItemSpinner(1, "ITRON"));
        marcas.add(new ItemSpinner(2, "ELSTER"));
        marcas.add(new ItemSpinner(3, "LANTA"));
        marcas.add(new ItemSpinner(4, "METER"));
        marcas.add(new ItemSpinner(5, "ELSGTM"));
        marcas.add(new ItemSpinner(6, "HOLBA"));
        marcas.add(new ItemSpinner(7, "ISKRA"));
        marcas.add(new ItemSpinner(8, "AEGMM"));
        marcas.add(new ItemSpinner(9, "ABB"));
        marcas.add(new ItemSpinner(10, "CDM"));
        marcas.add(new ItemSpinner(11, "TECMA"));
        marcas.add(new ItemSpinner(12, "METMA"));
        marcas.add(new ItemSpinner(13, "CHIMA"));
        marcas.add(new ItemSpinner(14, "KRISIK"));
        marcas.add(new ItemSpinner(15, "OSAMA"));
        marcas.add(new ItemSpinner(16, "ELSBA"));
        marcas.add(new ItemSpinner(17, "LAMBA"));
        marcas.add(new ItemSpinner(18, "CNM"));
        marcas.add(new ItemSpinner(19, "ESCMA"));


        return marcas;
    }

    public int slectedMarca(String marca)
    {
        String nombre = "";
        for (int i = 0; i < marcas.size() ; i++) {
            ItemSpinner itemTem = (ItemSpinner) marcas.get(i);
            nombre = itemTem.nombre;
            if(marca.equals(nombre))
            {
                return  i;
            }
        }
        return 0;
    }

    public int slectedClase(String clase)
    {
        String nombre = "";
        for (int i = 0; i < clasesMedidor.size() ; i++) {
            ItemSpinner itemTem = (ItemSpinner) clasesMedidor.get(i);
            nombre = itemTem.nombre;
            if(clase.equals(nombre))
            {
                return  i;
            }
        }
        return 0;
    }

    public int slectedTipo(String tipo)
    {
        String nombre = "";
        for (int i = 0; i < tipoMedidor.size() ; i++) {
            ItemSpinner itemTem = (ItemSpinner) tipoMedidor.get(i);
            nombre = itemTem.nombre;
            if(tipo.equals(nombre))
            {
                return  i;
            }
        }
        return 0;
    }
}
