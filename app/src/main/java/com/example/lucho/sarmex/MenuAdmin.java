package com.example.lucho.sarmex;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.example.lucho.sarmex.modelo.carga.ListaCargas;
import com.example.lucho.sarmex.modelo.funcionario.AdaptadorFuncionario;
import com.example.lucho.sarmex.modelo.funcionario.Funcionario;
import com.example.lucho.sarmex.modelo.funcionario.ListaFuncionarios;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

public class MenuAdmin extends AppCompatActivity {

    Button btnFuncionarios;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_admin);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        btnFuncionarios = (Button) findViewById(R.id.btnFuncionarios);
        btnFuncionarios.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent res = new Intent(MenuAdmin.this, ListaFuncionarios.class);
                startActivity(res);
            }
        });
    }


}
