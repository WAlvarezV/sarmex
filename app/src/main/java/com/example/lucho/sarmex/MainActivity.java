package com.example.lucho.sarmex;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

import com.example.lucho.sarmex.modelo.carga.ListaCargas;

public class MainActivity extends AppCompatActivity {


    String usuario, pass;

    final String NAMESPACE = "http://sgoliver.net/";
    final String URL = "http://"+Constantes.IP+"/SarmexWS.asmx";

    Button btnIngresar;
    EditText txtUsuario, txtPass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtUsuario = (EditText) findViewById(R.id.txtUsuario);
        txtPass = (EditText) findViewById(R.id.txtPassword);
        btnIngresar = (Button) findViewById(R.id.btnIngresar);

        btnIngresar.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                usuario = txtUsuario.getText().toString();
                pass = txtPass.getText().toString();

                if (usuario != "" && pass != "") {
                    ConsultaLogin consultaLogin = new ConsultaLogin();
                    consultaLogin.execute();
                }
            }
        });
    }

    // Tarea Asíncrona para llamar al WS de consulta en segundo plano
    private class ConsultaLogin extends AsyncTask<Void, Void, Integer> {

        protected Integer doInBackground(Void... params) {

            int resul = -1;

            final String METHOD_NAME = "loginUsuario";
            final String SOAP_ACTION = "http://sgoliver.net/loginUsuario";

            SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);

            request.addProperty("usuario", usuario);
            request.addProperty("clave", pass);

            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
                    SoapEnvelope.VER11);
            envelope.dotNet = true;

            envelope.setOutputSoapObject(request);

            HttpTransportSE transporte = new HttpTransportSE(URL);

            try {
                transporte.call(SOAP_ACTION, envelope);
                SoapPrimitive resultado_xml = (SoapPrimitive) envelope.getResponse();
                String res = resultado_xml.toString();
                if (res.equals("1"))
                {
                    resul = Integer.parseInt(res);
                }
                else if(!res.equals("-1"))
                {
                    resul = Integer.parseInt(res);
                }
            } catch (Exception e) {
                Log.i("Errores", e.toString());
            }
            return resul;
        }

        protected void onPostExecute(Integer result) {
            Intent res;
            if (result == 1) {
                res = new Intent(MainActivity.this, MenuAdmin.class);
                startActivity(res);
            }
            else if (result != -1) {
                res = new Intent(MainActivity.this, ListaCargas.class);
                res.putExtra("ID", result);
                startActivity(res);
            }
            else {
                AlertDialog.Builder alerta = new AlertDialog.Builder(MainActivity.this);
                alerta.setTitle("Error: ");
                alerta.setMessage("Los datos proporsionados no son validos");
                alerta.setPositiveButton("Atras", null);
                alerta.show();

            }
        }
    }
}